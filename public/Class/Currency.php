<?php


class Currency
{
    private $isoCode;

    public function __construct($iso)
    {
        $this->setISO($iso);
    }

    private function setISO($iso)
    {
        if (strlen("$iso") != "3") {
            throw new InvalidArgumentException("Eror format");
        } elseif (!ctype_upper($iso)) {
            throw new InvalidArgumentException("Eror format");
        }
        $this->isoCode = $iso;
    }

    public function getISO()
    {
        return $this->isoCode;
    }

    public function equals(Currency $isoCode)
    {
        if ($this->isoCode === $isoCode->getISO()) {
            return true;
        }
        return false;
    }
}