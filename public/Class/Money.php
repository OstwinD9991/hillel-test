<?php


class Money
{
    private $amount;
    private $currency;

    public function __construct($amount_money)
    {
        $this->setamount($amount_money);
    }

    private function setamount($amount_money)
    {
        $this->amount = $amount_money;
    }

    public function getamount()
    {
        return $this->amount;
    }

    public function setcurrency(Currency $isoCode)
    {
        $this->currency = $isoCode->getISO();
    }

    public function getcurrency()
    {
        return $this->currency;
    }

    public function equalsamount(Money $currency, $amount)
    {
        if ($this->currency === $currency->getcurrency()) {
            if ($this->amount === $amount->getamount()) {
                return true;
            }
        }
        return false;
    }

    public function add(Money $amount, $currency)
    {
        if ($this->currency !== $currency->getcurrency()) {
            throw new InvalidArgumentException("Eror format");
        }
        $add= $this->amount + $amount->getamount();
        return $add;

    }
}